<p align="center">
   <img src="https://img.shields.io/badge/platforms-android-blueviolet?style=for-the-badge"/>
   <a href="https://github.com/RepoDevil/Himitsu/releases"><img src="https://img.shields.io/github/actions/workflow/status/RepoDevil/Himitsu/android.yml?color=%233DDC84&logo=android&logoColor=%23fff&style=for-the-badge"></a>
   <a href="https://www.codefactor.io/repository/github/RepoDevil/Himitsu"><img src="https://www.codefactor.io/repository/github/RepoDevil/Himitsu/badge?color=%233DDC84&logo=android&logoColor=%23fff&style=for-the-badge" alt="CodeFactor" /></a>
</p>
<p align="center">
   <a href="https://discord.gg/vnrhgrt"><img src="https://invidget.switchblade.xyz/vnrhgrt"></a>
</p>

# **Himitsu** ㊙️

> **Himitsu (秘密; Him-itsu)** means "secret" in Japanese. *Shhh*

<br />
<i>
This project was forked from Dantotsu under the <a href="LICENSE.md">GNU General Public License v3.0</a> and severed all ties upon a license change. As a license cannot be applied retroactively, this project is only subject to the terms of the original license.
<br />
Portions of this code are also license under the <a href="LICENSE.md#L680">Apache License, Version 2.0</a>. Due to negligence on behalf of Dantotsu, the specific history of some code has been destroyed. This code, along with any submodules or integrations, is used with permission according to the individual license terms and conditions. Any permission granted by Himitsu does not waive the restrictions set forth by these licenses.
<br />
If you have any questions or feel this project is in violation of your license, please do not hesitate to contact the Discord channel above.
</i>
<br />

<p align="center">
<img src="https://count.getloli.com/get/@:repodevildantotsu" alt=":repodevildantotsu" />
</p>
