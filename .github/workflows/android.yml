name: Matagi Dispatch

on:
  push:
    branches: [ matagi, himitsu ]
    paths-ignore:
      - '**/README.md'
  workflow_dispatch:
    inputs:
      release:
        description: 'Release'
        required: true
        type: boolean
        default: false

permissions: write-all

concurrency:
  group: ${{ github.workflow }}-${{ github.ref }}
  cancel-in-progress: true

jobs:
  soak:
    runs-on: ubuntu-latest
    steps:
      - name: Execute himitsu dispatch
        if: contains(github.ref, 'himitsu')
        uses: peter-evans/repository-dispatch@v3
        with:
          token: ${{ secrets.ORG_TOKEN }}
          repository: RepoDevil/Himitsu-Ichimoku
          event-type: himitsu

      - name: Execute matagi dispatch
        if: contains(github.ref, 'matagi')
        uses: peter-evans/repository-dispatch@v3
        with:
          token: ${{ secrets.ORG_TOKEN }}
          repository: RepoDevil/Himitsu
          event-type: matagi

      - name: Purge past workflows
        uses: Mattraks/delete-workflow-runs@v2
        with:
          token: ${{ secrets.GITHUB_TOKEN }}
          repository: ${{ github.repository }}
          retain_days: 1
          keep_minimum_runs: 2

  rinse:
    if: github.event.inputs.release
    runs-on: ubuntu-latest
    env:
      CI: true
    steps:

    - name: Drop orphan tags
      continue-on-error: true
      uses: fabriziocacicia/delete-tags-without-release-action@v0.1.0
      env:
        GITHUB_TOKEN: ${{ secrets.GITHUB_TOKEN }}

  wash:
    needs: rinse
    runs-on: ubuntu-latest
    env:
      CI: true

    steps:
    - name: Checkout repo
      uses: actions/checkout@v4
      with:
        repository: RepoDevil/Himitsu-Matagi
        token: ${{ secrets.ORG_TOKEN }}
        fetch-depth: 0

    - name: Retrieve commit hash
      run: |
        GIT_HASH=$(git rev-parse --short HEAD)
        echo "GIT_HASH=${GIT_HASH}" >> $GITHUB_ENV

    - name: Retrieve update token
      run: |
        mkdir app/src/main/res/raw
        echo ${{ secrets.ORG_TOKEN }} > app/src/main/res/raw/token.api

    - name: Download server.aar
      shell: bash
      run: |
        if [ ! -d app/libs ]; then
          mkdir app/libs
        fi
        curl -H "Authorization: Token ${{ secrets.ORG_TOKEN }}" \
        -H 'Accept: application/vnd.github.v3.raw' \
        -o "app/libs/server.aar" \
        -L "https://api.github.com/repos/RepoDevil/TorrServer/contents/server.aar"

    - name: Download server-sources.jar
      shell: bash
      run: |
        curl -H "Authorization: Token ${{ secrets.ORG_TOKEN }}" \
        -H 'Accept: application/vnd.github.v3.raw' \
        -o "app/libs/server-sources.jar" \
        -L "https://api.github.com/repos/RepoDevil/TorrServer/contents/server-sources.jar"

    - name: Setup JDK 17
      uses: actions/setup-java@v4
      with:
        distribution: 'temurin'
        java-version: 17
        cache: gradle

    - name: Decode Keystore File
      run: echo "${{ secrets.KEYSTORE_FILE }}" | base64 -d > $GITHUB_WORKSPACE/key.keystore

    - uses: burrunan/gradle-cache-action@v1
      name: Build Release Gradle
      with:
        job-id: Release
        # Specifies arguments for Gradle execution
        arguments: |
          --configure-on-demand
          assembleGoogleMatagi
          -Pandroid.injected.signing.store.file=${{ github.workspace }}/key.keystore
          -Pandroid.injected.signing.store.password=${{ secrets.KEYSTORE_PASSWORD }}
          -Pandroid.injected.signing.key.alias=${{ secrets.KEY_ALIAS }}
          -Pandroid.injected.signing.key.password=${{ secrets.KEY_PASSWORD }}
        # Gradle version to use for execution:
        #   wrapper (default), current, rc, nightly, release-nightly, or
        #   versions like 6.6 (see https://services.gradle.org/versions/all)
        gradle-version: wrapper
        # Properties are passed as -Pname=value
        properties: |
          org.gradle.unsafe.configuration-cache=true

    - name: Upload a Build Artifact
      uses: actions/upload-artifact@v4.3.1
      with:
          name: Himitsu ${{ env.GIT_HASH }}
          retention-days: 1
          path: "app/build/outputs/apk/google/matagi/Himitsu-${{ env.GIT_HASH }}-google-arm64-v8a-matagi.apk"

    - name: Purge past releases
      uses: keyfactor/action-delete-prereleases@main
      with:
          repo-token: ${{ secrets.GITHUB_TOKEN }}

    - name: Purge past workflows
      uses: Mattraks/delete-workflow-runs@v2
      with:
        token: ${{ secrets.GITHUB_TOKEN }}
        repository: ${{ github.repository }}
        retain_days: 1
        keep_minimum_runs: 2

    - uses: 8bitDream/action-github-releases@v1.0.0
      with:
        repo_token: "${{ secrets.GITHUB_TOKEN }}"
        automatic_release_tag: ${{ env.GIT_HASH }}
        prerelease: true
        title: Himitsu ${{ env.GIT_HASH }}
        files: |
          app/build/outputs/apk/google/matagi/Himitsu-${{ env.GIT_HASH }}-google-universal-matagi.apk
          app/build/outputs/apk/google/matagi/Himitsu-${{ env.GIT_HASH }}-google-arm64-v8a-matagi.apk
          app/build/outputs/apk/google/matagi/Himitsu-${{ env.GIT_HASH }}-google-armeabi-v7a-matagi.apk
          app/build/outputs/apk/google/matagi/Himitsu-${{ env.GIT_HASH }}-google-x86_64-matagi.apk
          app/build/outputs/apk/google/matagi/Himitsu-${{ env.GIT_HASH }}-google-x86-matagi.apk

  dry:
    needs: wash
    runs-on: ubuntu-latest
    env:
      CI: true
    steps:

    - name: Purge build cache
      continue-on-error: true
      uses: MyAlbum/purge-cache@v2
      with:
        max-age: 86400 # 1 day, default: 7 days since last use
        token: ${{ secrets.GITHUB_TOKEN }}
